import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link"  to="/customer/new/" id="create-customer-form">Create a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/customers/" id="customer-link">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/salesperson/new/" id="create-salesperson-form">Create a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/salesperson/" id="salesperson-link">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/sale/new/" id="create-sale-form">Create a Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/sales/" id="sales-link">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/sales/history/" id="sales-history">Sales History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/manufacturer/new/" id="create-manufacturer-form">Create Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/manufacturers/" id="manufacturer-link">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/models/new/" id="create-model-form">Create a Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/models/" id="models-link">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/automobile/new/" id="create-automobile-form">Create a Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/automobile/" id="automobile-link">Automobiles</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

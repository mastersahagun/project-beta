import React, { useEffect, useState } from 'react';

function SalesList(props) {
    const [sales, setSale] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8090/api/sales/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSale(data.sales);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8090/api/sales/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setSale(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Customer</th>
                    <th>Salesperson</th>
                    <th>Employee ID</th>
                    <th>Automobile</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                    <tr key={sale.id}>
                        <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                        <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                        <td>{ sale.salesperson.employee_id }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>${ sale.price }</td>
                        <td><button onClick={() => handleDelete(sale.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default SalesList;

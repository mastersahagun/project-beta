import React, { useEffect, useState } from 'react';

function CustomerList(props) {
    const [customers, setCustomer] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8090/api/customers`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8090/api/customers/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setCustomer(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customers.map(customer => {
                    return (
                    <tr key={customer.id}>
                        <td>{ customer.first_name }</td>
                        <td>{ customer.last_name }</td>
                        <td>{ customer.address }</td>
                        <td>{ customer.phone_number }</td>
                        <td><button onClick={() => handleDelete(customer.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default CustomerList;

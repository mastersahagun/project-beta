import React, { useEffect, useState } from 'react';

function ModelsForm() {
    const [manufacturers, setManufacturer] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const fetchData = async () => {
        const url = `http://localhost:8100/api/manufacturers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/models/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
            event.target.reset();
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Car URL" required type="url" id="picture_url" name="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Model Image URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="manufacturer_id" name="manufacturer_id" className="form-select">
                            <option>Choose a Manufacturer</option>
                            {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default ModelsForm;

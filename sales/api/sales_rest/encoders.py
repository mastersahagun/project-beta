from common.json import ModelEncoder
from .models import Customer, AutomobileVO, Salesperson, Sale


class AutoDetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]


class SalesPersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
        "customer",
        "salesperson",
        "automobile",
        ]
    encoders = {
        "customer": CustomerEncoder(),
        "salesperson": SalesPersonEncoder(),
        "automobile": AutoDetailEncoder(),
    }

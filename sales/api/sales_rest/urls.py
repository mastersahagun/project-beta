from django.urls import path
from .views import SalesPersonView, SalesPersonDetails, CustomersView, CustomerDetails, SalesView, SalesDetails

urlpatterns = [
    path("salespeople/", SalesPersonView),
    path("salespeople/<int:pk>/", SalesPersonDetails),
    path("customers/", CustomersView),
    path("customers/<int:pk>/", CustomerDetails),
    path("sales/", SalesView),
    path("sales/<int:pk>/", SalesDetails),
]

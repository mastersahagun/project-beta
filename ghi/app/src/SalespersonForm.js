import React, { useState } from 'react';

function SalespersonForm() {
    const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespeople/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
            event.target.reset();
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Customer</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="first_name" required type="text" id="first_name" name="first_name" className="form-control"/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="last_name" required type="text" id="last_name" name="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="employee_id" type="number" id="employee_id" name="employee_id" className="form-control"/>
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default SalespersonForm;

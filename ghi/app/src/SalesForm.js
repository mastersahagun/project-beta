import React, { useEffect, useState } from 'react';

function SalesForm() {
    const [automobile, setAutomobile] = useState([])
    const [salesperson, setSalesperson] = useState([])
    const [customers, setCustomer] = useState([])
    const [formData, setFormData] = useState({
        customer: '',
        salesperson: '',
        automobile: '',
        price: '',
    })

    const fetchAutoData = async () => {
        const url = `http://localhost:8100/api/automobiles/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.autos);
        }
    }

    useEffect(() => {
        fetchAutoData();
    }, []);

    const fetchCustomerData = async () => {
        const url = `http://localhost:8090/api/customers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers);
        }
    }

    useEffect(() => {
        fetchCustomerData();
    }, []);

    const fetchSalespersonData = async () => {
        const url = `http://localhost:8090/api/salespeople/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salesPerson);
        }
    }

    useEffect(() => {
        fetchSalespersonData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/sales/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                customer: '',
                salesperson: '',
                automobile: '',
                price: '',
            });
            event.target.reset();
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const filtered = automobile.filter((automobile) => automobile.sold === false);

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="customer" name="customer" className="form-select">
                            <option>Choose a Customer</option>
                            {customers.map(customer => {
                    return (
                        <option key={customer.phone_number} value={customer.phone_number}>{customer.first_name} {customer.last_name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="salesperson" name="salesperson" className="form-select">
                            <option>Choose a Salesperson</option>
                            {salesperson.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="automobile" name="automobile" className="form-select">
                            <option>Choose a Automobile</option>
                            {filtered.map(automobile => {
                    return (
                        <option key={automobile.vin} value={automobile.vin}>{automobile.model.name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="price" required type="number" id="price" name="price" className="form-control"/>
                        <label htmlFor="price">Car Cost</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default SalesForm;

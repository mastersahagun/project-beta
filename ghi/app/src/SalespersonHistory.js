import React, { useEffect, useState } from 'react';

function SalesHistory(props) {
    const [sales, setSale] = useState([])
    const [employees, setEmployee] = useState([])
    const [employeeID, setEmployeeID] = useState('')
    const fetchSalesData = async () => {
        const url = `http://localhost:8090/api/sales/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSale(data.sales);
        }
    }

    useEffect(() => {
        fetchSalesData();
    }, []);

    const fetchEmployeeData = async () => {
        const url = `http://localhost:8090/api/salespeople/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setEmployee(data.salesPerson);
        }
    }

    useEffect(() => {
        fetchEmployeeData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        setEmployeeID(
            value
        );
    }

    const filtered = sales.filter((sale) => sale.salesperson.employee_id == employeeID);
    console.log(sales)
    return (
        <div>
        <div className="mb-3">
            <select onChange={handleFormChange} required id="filtered" name="automobile" className="form-select">
                <option>Choose a Salesperson</option>
                {employees.map(employee => {
        return (
            <option key={employee.employee_id} value={employee.employee_id}>{employee.first_name} {employee.last_name}</option>
        )
        })}
            </select>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Customer</th>
                    <th>Automobile</th>
                    <th>Price</th>
                </tr>
            </thead>
            {filtered.map(sale => {
                return (
                <tr key={sale.id}>
                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>${ sale.price }</td>
                </tr>
                );
            })}
        </table>
        </div>
    );
    }

    export default SalesHistory;

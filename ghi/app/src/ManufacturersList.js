import React, { useEffect, useState } from 'react';

function ManufacturersList(props) {
    const [manufacturers, setManufacturers] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8100/api/manufacturers/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8100/api/manufacturers/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setManufacturers(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                        <td><button onClick={() => handleDelete(manufacturer.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default ManufacturersList;

import React, { useEffect, useState } from 'react';

function ModelsList(props) {
    const [models, setModels] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8100/api/models/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8100/api/models/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setModels(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                    <tr key={model.id}>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name }</td>
                        <td><img src ={model.picture_url} alt="car" height="100"/></td>
                        <td><button onClick={() => handleDelete(model.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default ModelsList;

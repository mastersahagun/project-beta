from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Customer, AutomobileVO, Salesperson, Sale
from .encoders import CustomerEncoder, SalesPersonEncoder, SalesEncoder


@require_http_methods(["GET", "POST"])
def SalesPersonView(request):

    if request.method == "GET":
        salesPerson = Salesperson.objects.all()
        return JsonResponse(
            {"salesPerson": salesPerson},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesPerson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesPerson,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def SalesPersonDetails(request, pk):
    if request.method == "GET":
        try:
            salesPersonDetails = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesPersonDetails,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesPersonDetails = Salesperson.objects.get(id=pk)
            salesPersonDetails.delete()
            return JsonResponse(
                salesPersonDetails,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesPersonDetails = Salesperson.objects.get(id=pk)

            props = [
                "first_name",
                "last_name",
                "employee_id",
                ]
            for prop in props:
                if prop in content:
                    setattr(salesPersonDetails, prop, content[prop])
            salesPersonDetails.save()
            return JsonResponse(
                salesPersonDetails,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def CustomersView(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            content,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def CustomerDetails(request, pk):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=pk)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customers = Customer.objects.get(id=pk)
            customers.delete()
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.get(id=pk)

            props = [
                "first_name",
                "last_name",
                "address",
                "phone_number",
                ]
            for prop in props:
                if prop in content:
                    setattr(customers, prop, content[prop])
            customers.save()
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def SalesView(request):

    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(
                employee_id=content["salesperson"],
                )
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales Person id"},
                status=400,
            )
        try:
            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer id"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def SalesDetails(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=pk)

            try:
                automobile = AutomobileVO.objects.get(
                    vin=content["automobile"],
                    )
                content["automobile"] = automobile
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid automobile id"},
                    status=400,
                )
            try:
                salesperson = Salesperson.objects.get(
                    employee_id=content["salesperson"],
                    )
                content["salesperson"] = salesperson
            except Salesperson.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Sales Person id"},
                    status=400,
                )
            try:
                customer = Customer.objects.get(
                    phone_number=content["customer"],
                    )
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Customer id"},
                    status=400,
                )
            props = [
                "id",
                "price",
                "salesperson",
                "customer",
                "automobile",
                ]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

import React, { useEffect, useState } from 'react';

function AutomobileList(props) {
    const [automobile, setAutomobile] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8100/api/automobiles/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8100/api/automobiles/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setAutomobile(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Vin</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {automobile.map(automobile => {
                    return (
                    <tr key={automobile.id}>
                        <td>{ automobile.color }</td>
                        <td>{ automobile.year }</td>
                        <td>{ automobile.vin }</td>
                        <td>{ automobile.model.name }</td>
                        <td><button onClick={() => handleDelete(automobile.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default AutomobileList;

import React, { useEffect, useState } from 'react';

function EmployeeList(props) {
    const [employees, setEmployee] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8090/api/salespeople/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setEmployee(data.salesPerson);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8090/api/salespeople/${id}`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setEmployee(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {employees.map(employee => {
                    return (
                    <tr key={employee.id}>
                        <td>{ employee.first_name }</td>
                        <td>{ employee.last_name }</td>
                        <td>{ employee.employee_id }</td>
                        <td><button onClick={() => handleDelete(employee.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default EmployeeList;

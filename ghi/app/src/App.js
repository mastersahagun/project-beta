import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalespersonForm from './SalespersonForm';
import EmployeeList from './SalespersonList';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalesHistory from './SalespersonHistory';
import ManufacturersForm from './ManufacturersForm';
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModelsList';
import ModelsForm from './ModelsForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customer/new/" element={<CustomerForm />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/salesperson/new/" element={<SalespersonForm />} />
          <Route path="/salesperson/" element={<EmployeeList />} />
          <Route path="/sale/new/" element={<SalesForm />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/history/" element={<SalesHistory />} />
          <Route path="/manufacturer/new/" element={<ManufacturersForm />} />
          <Route path="/manufacturers/" element={<ManufacturersList />} />
          <Route path="/models/new/" element={<ModelsForm />} />
          <Route path="/models/" element={<ModelsList />} />
          <Route path="/automobile/new/" element={<AutomobileForm />} />
          <Route path="/automobile/" element={<AutomobileList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
